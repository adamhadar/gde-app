import 'package:flutter/material.dart';
import 'package:gde_app/widgets/login_screen_widgets/login_form.dart';
import 'package:gde_app/widgets/logo_widget.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: size.height * .32,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.vertical(
                        bottom: Radius.elliptical(
                          size.width / 2,
                          size.height * .05,
                        ),
                      ),
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  const SafeArea(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Vitajte",
                          style: TextStyle(
                            fontSize: 76,
                            height: 1,
                            color: Colors.white,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                        Text(
                          "Prihláste sa do dvojho účtu",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        LogoWidget(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const LoginFormBody(),
          ],
        ),
      ),
    );
  }
}
