import 'package:flutter/material.dart';

import 'package:gde_app/main.dart';
import 'package:gde_app/models/localization_request.dart';
import 'package:gde_app/providers/requests.dart';
import 'package:gde_app/widgets/home_screen_widgets/my_icon_button.dart';
import 'package:gde_app/widgets/logo_widget.dart';
import 'package:gde_app/widgets/request_detail_screen_widgets/comment_create_widget.dart';
import 'package:gde_app/widgets/request_detail_screen_widgets/comments_list_view.dart';

import 'package:provider/provider.dart';

class RequestDetailScreen extends StatelessWidget {
  const RequestDetailScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final requestId = ModalRoute.of(context)?.settings.arguments as String;
    final requestProvider = Provider.of<LocalizationRequestProvider>(context);
    final LocalizationRequest? request = requestProvider.requests
        .map((e) => e as LocalizationRequest?)
        .firstWhere(
          (element) => element?.id == requestId,
          orElse: () => null,
        );

    if (request == null) {
      return const Center(
        child: Text(
          "Žiadosť nebola nájdená",
          style: TextStyle(fontSize: 20, color: Colors.red),
        ),
      );
    }

    debugPrint(requestId.toString());
    debugPrint(request.toString());
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: RefreshIndicator(
            onRefresh: () async =>
                await Provider.of<LocalizationRequestProvider>(
              context,
              listen: false,
            ).fetchComments(),
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Column(
                children: [
                  Row(
                    children: [
                      const SizedBox(width: 10),
                      MyIconButton(
                          icon: Icons.arrow_back_outlined,
                          onTap: () => navigationKey.currentState!.pop()),
                      const Spacer(),
                      const LogoWidget(),
                      const SizedBox(width: 10),
                    ],
                  ),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 2,
                        ),
                      ],
                    ),
                    margin: const EdgeInsets.all(20),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              flex: 8,
                              child: Text(
                                request.title,
                                textAlign: TextAlign.left,
                                style: const TextStyle(
                                  fontSize: 32,
                                  letterSpacing: -2,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            const Spacer(
                              flex: 1,
                            ),
                            if (request.isSolved)
                              const Expanded(
                                flex: 1,
                                child: Icon(
                                  Icons.check_circle,
                                  color: Colors.green,
                                  size: 40,
                                ),
                              ),
                          ],
                        ),
                        const SizedBox(height: 10),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(16),
                          child: Image.network(
                            request.pictureUrl,
                            width: double.infinity,
                            height: 250,
                            fit: BoxFit.cover,
                          ),
                        ),
                        const SizedBox(height: 10),
                        const SizedBox(
                          width: double.infinity,
                          child: Text(
                            textAlign: TextAlign.left,
                            "Popis:",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w800,
                              letterSpacing: -2,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: Text(
                            textAlign: TextAlign.left,
                            request.description,
                            style: const TextStyle(
                              fontSize: 18,
                              letterSpacing: -1,
                            ),
                          ),
                        ),
                        const SizedBox(height: 20),
                        const SizedBox(
                          width: double.infinity,
                          child: Text(
                            textAlign: TextAlign.left,
                            "Komentáre:",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w800,
                              letterSpacing: -2,
                            ),
                          ),
                        ),
                        CommentListView(request: request),
                        CommentCreateWidget(request)
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
