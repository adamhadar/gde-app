import 'package:flutter/material.dart';
import 'package:gde_app/main.dart';
import 'package:gde_app/models/filter_options.dart';
import 'package:gde_app/providers/requests.dart';
import 'package:gde_app/widgets/home_screen_widgets/nav_bar_row.dart';
import 'package:gde_app/widgets/home_screen_widgets/requests_list_view.dart';
import 'package:gde_app/widgets/primary_color_text_button.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});
  @override
  State<StatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await Provider.of<LocalizationRequestProvider>(context, listen: false)
          .fetchRequests();
    });
  }

  @override
  Widget build(BuildContext context) {
    final requestProvider = Provider.of<LocalizationRequestProvider>(context);
    return Scaffold(
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: () async => await requestProvider.fetchRequests(),
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              children: [
                const NavBarRow(),
                const SizedBox(height: 20),
                if (requestProvider.filtering)
                  Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          const SizedBox(width: 5),
                          const Text("Zobraziť vyriešené"),
                          const Spacer(),
                          Checkbox(
                            value: requestProvider.filterOptions.includeSolved,
                            onChanged: (value) =>
                                requestProvider.setFilterOptions(
                              value,
                              null,
                            ),
                          ),
                          const SizedBox(width: 5),
                        ],
                      ),
                      Container(
                        padding: const EdgeInsets.all(5),
                        width: double.infinity,
                        child: const Text("Zoradiť podľa:"),
                      ),
                      ButtonBar(
                        alignment: MainAxisAlignment.spaceAround,
                        children: [
                          TextButton(
                            style: ButtonStyle(
                              foregroundColor:
                                  MaterialStateProperty.all(Colors.black),
                              backgroundColor:
                                  MaterialStateProperty.resolveWith(
                                (states) =>
                                    requestProvider.filterOptions.orderType ==
                                            OrderType.newest
                                        ? Theme.of(context)
                                            .primaryColor
                                            .withOpacity(.5)
                                        : Colors.transparent,
                              ),
                            ),
                            onPressed: () => requestProvider.setFilterOptions(
                              null,
                              OrderType.newest,
                            ),
                            child: const Text("Najnovšie"),
                          ),
                          TextButton(
                            style: ButtonStyle(
                              foregroundColor:
                                  MaterialStateProperty.all(Colors.black),
                              backgroundColor:
                                  MaterialStateProperty.resolveWith(
                                (states) =>
                                    requestProvider.filterOptions.orderType ==
                                            OrderType.oldest
                                        ? Theme.of(context)
                                            .primaryColor
                                            .withOpacity(.5)
                                        : Colors.transparent,
                              ),
                            ),
                            onPressed: () => requestProvider.setFilterOptions(
                              null,
                              OrderType.oldest,
                            ),
                            child: const Text("Najstaršie"),
                          ),
                          TextButton(
                            style: ButtonStyle(
                              foregroundColor:
                                  MaterialStateProperty.all(Colors.black),
                              backgroundColor:
                                  MaterialStateProperty.resolveWith(
                                (states) =>
                                    requestProvider.filterOptions.orderType ==
                                            OrderType.mostDiscussed
                                        ? Theme.of(context)
                                            .primaryColor
                                            .withOpacity(.5)
                                        : Colors.transparent,
                              ),
                            ),
                            onPressed: () => requestProvider.setFilterOptions(
                              null,
                              OrderType.mostDiscussed,
                            ),
                            child: const Text("Najdiskutovanejšie"),
                          ),
                        ],
                      )
                    ],
                  ),
                PrimaryColorTextButton(
                  text: "Nová žiadosť",
                  onPressed: () {
                    navigationKey.currentState!.pushNamed('/new');
                  },
                ),
                if (requestProvider.isFetching)
                  const Padding(
                    padding: EdgeInsets.all(36.0),
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                if (!requestProvider.isFetching)
                  RequestListView(requestProvider)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
