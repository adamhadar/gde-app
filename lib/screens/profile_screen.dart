import 'package:flutter/material.dart';
import 'package:gde_app/main.dart';
import 'package:gde_app/models/localization_options.dart';
import 'package:gde_app/providers/auth.dart';
import 'package:gde_app/providers/requests.dart';
import 'package:gde_app/providers/storage.dart';
import 'package:gde_app/widgets/home_screen_widgets/my_icon_button.dart';
import 'package:gde_app/widgets/home_screen_widgets/requests_list_view.dart';
import 'package:gde_app/widgets/logo_widget.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final TextEditingController _usernameController = TextEditingController();

  @override
  void initState() {
    _usernameController.text = Provider.of<AuthProvider>(
      context,
      listen: false,
    ).getUsername;
    super.initState();
  }

  @override
  void dispose() {
    _usernameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<LocalizationRequestProvider>(context);
    final authProvider = Provider.of<AuthProvider>(context);
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    const SizedBox(width: 10),
                    MyIconButton(
                        icon: Icons.arrow_back_outlined,
                        onTap: () => navigationKey.currentState!.pop()),
                    const SizedBox(width: 10),
                    MyIconButton(
                      icon: Icons.check_outlined,
                      onTap: () async {
                        await authProvider
                            .updateUsername(_usernameController.text);
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            backgroundColor: Colors.green,
                            content: Text('Úspešne zmennené'),
                          ),
                        );
                        navigationKey.currentState!.pop();
                      },
                    ),
                    const SizedBox(width: 10),
                    const Spacer(),
                    const LogoWidget(),
                    const SizedBox(width: 10),
                  ],
                ),
                const SizedBox(height: 20),
                Stack(
                  alignment: Alignment.topCenter,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 50),
                      height: 100,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            blurRadius: 10,
                          ),
                        ],
                        color: Colors.white,
                        borderRadius: const BorderRadius.only(
                          bottomLeft: Radius.circular(20),
                          bottomRight: Radius.circular(20),
                        ),
                      ),
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: EditableText(
                          focusNode: FocusNode(),
                          cursorColor: Theme.of(context).primaryColor,
                          backgroundCursorColor: Colors.white,
                          controller: _usernameController,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 24,
                          ),
                        ),
                      ),
                    ),
                    CircleAvatar(
                      radius: 50,
                      backgroundImage: FadeInImage(
                        placeholder: Image.network(
                          StorageProvider.placeholderPfpUrl,
                        ).image,
                        image: Image.network(
                          authProvider.getPfpUrl,
                        ).image,
                      ).image,
                    ),
                  ],
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  width: double.infinity,
                  child: const Text(
                    "Vaše príspevky:",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                    ),
                  ),
                ),
                RequestListView(
                  provider,
                  options: LocalizationOptions(
                    ownerId: authProvider.getUID,
                  ),
                ),
                const SizedBox(height: 20),
                Text(
                  "Členom od ${DateFormat('dd.MM.yyyy').format(authProvider.getCurrentUser()!.metadata.creationTime!)}",
                ),
                const SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
