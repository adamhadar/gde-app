import 'package:flutter/material.dart';
import 'package:gde_app/main.dart';
import 'package:gde_app/providers/requests.dart';
import 'package:gde_app/widgets/home_screen_widgets/my_icon_button.dart';
import 'package:gde_app/widgets/logo_widget.dart';
import 'package:provider/provider.dart';

class LeaderboardScreen extends StatefulWidget {
  const LeaderboardScreen({super.key});

  @override
  State<LeaderboardScreen> createState() => _LeaderboardScreenState();
}

class _LeaderboardScreenState extends State<LeaderboardScreen> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) async {
        await Provider.of<LocalizationRequestProvider>(context, listen: false)
            .fetchLeaderboard();
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final requestProvider = Provider.of<LocalizationRequestProvider>(context);
    return Scaffold(
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: () async {
            await Provider.of<LocalizationRequestProvider>(context,
                    listen: false)
                .fetchLeaderboard();
          },
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              children: [
                Row(
                  children: [
                    const SizedBox(width: 10),
                    MyIconButton(
                        icon: Icons.arrow_back_outlined,
                        onTap: () => navigationKey.currentState!.pop()),
                    const Spacer(),
                    const LogoWidget(),
                    const SizedBox(width: 10),
                  ],
                ),
                const SizedBox(height: 20),
                const SizedBox(
                  height: 200,
                  width: double.infinity,
                  child: Center(
                    child: Text(
                      'Rebríček najviac vyriešených žiadostí',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 28,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: requestProvider.leaderboard.length,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (ctx, idx) {
                    final user = requestProvider.leaderboard[idx];
                    return Container(
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius:
                            idx == 0 ? BorderRadius.circular(10) : null,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Row(children: [
                          CircleAvatar(
                            backgroundImage:
                                NetworkImage(user.profilePictureUrl),
                          ),
                          const SizedBox(width: 10),
                          Text(user.username),
                          const Spacer(),
                          Text(user.numberOfRequestsSolved.toString()),
                        ]),
                      ),
                    );
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
