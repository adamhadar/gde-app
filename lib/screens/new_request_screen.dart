import 'package:flutter/material.dart';
import 'package:gde_app/main.dart';
import 'package:gde_app/widgets/home_screen_widgets/my_icon_button.dart';
import 'package:gde_app/widgets/logo_widget.dart';
import 'package:gde_app/widgets/new_requrest_screen_widgets/new_requrest_form.dart';

class NewRequestScreen extends StatelessWidget {
  const NewRequestScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    const SizedBox(width: 10),
                    MyIconButton(
                        icon: Icons.arrow_back_outlined,
                        onTap: () => navigationKey.currentState!.pop()),
                    const Spacer(),
                    const LogoWidget(),
                    const SizedBox(width: 10),
                  ],
                ),
                const SizedBox(height: 20),
                const NewRequestForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
