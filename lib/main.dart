import 'package:flutter/material.dart';
import 'package:gde_app/providers/auth.dart';
import 'package:gde_app/providers/requests.dart';
import 'package:gde_app/providers/storage.dart';
import 'package:gde_app/screens/home_screen.dart';
import 'package:gde_app/screens/leaderboard_screen.dart';
import 'package:gde_app/screens/login_screen.dart';
import 'package:gde_app/screens/new_request_screen.dart';
import 'package:gde_app/screens/profile_screen.dart';
import 'package:gde_app/screens/request_detail_screen.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const GDEApp());
}

final GlobalKey<NavigatorState> navigationKey = GlobalKey<NavigatorState>();

class GDEApp extends StatelessWidget {
  const GDEApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthProvider()),
        ChangeNotifierProvider(create: (_) => StorageProvider()),
        ChangeNotifierProvider(create: (_) => LocalizationRequestProvider()),
      ],
      child: MaterialApp(
        navigatorKey: navigationKey,
        title: 'GDE app',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: 'Sen',
          colorScheme: const ColorScheme.light(
            primary: Color.fromRGBO(239, 213, 122, 1),
            background: Colors.white,
            error: Color.fromRGBO(227, 64, 64, 1),
          ),
          useMaterial3: true,
        ),
        routes: {
          '/': (ctx) => const LoginScreen(),
          '/home': (ctx) => const HomeScreen(),
          '/new': (ctx) => const NewRequestScreen(),
          '/request': (ctx) => const RequestDetailScreen(),
          '/profile': (ctx) => const ProfileScreen(),
          '/leaderboard': (ctx) => const LeaderboardScreen(),
        },
      ),
    );
  }
}
