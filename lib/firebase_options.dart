// File generated by FlutterFire CLI.
// ignore_for_file: type=lint
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        return windows;
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyA44Vhja2zn8vyEmS0lytKIXngIRnsVFmU',
    appId: '1:785781696031:web:21182a9688486dc618972a',
    messagingSenderId: '785781696031',
    projectId: 'gde-app-f74ff',
    authDomain: 'gde-app-f74ff.firebaseapp.com',
    storageBucket: 'gde-app-f74ff.appspot.com',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyBPZ7jsYxQAKQgNv_s2x5B7dM6PhD-9UPs',
    appId: '1:785781696031:android:820b18985f693c2e18972a',
    messagingSenderId: '785781696031',
    projectId: 'gde-app-f74ff',
    storageBucket: 'gde-app-f74ff.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyAJpZ0smrLQrEysBtRyro9iaTETIgnf8VU',
    appId: '1:785781696031:ios:421b853679b64e5118972a',
    messagingSenderId: '785781696031',
    projectId: 'gde-app-f74ff',
    storageBucket: 'gde-app-f74ff.appspot.com',
    iosClientId: '785781696031-be4ndrad8nsb6lcjio08sf38tghp24sk.apps.googleusercontent.com',
    iosBundleId: 'com.example.gdeApp',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyAJpZ0smrLQrEysBtRyro9iaTETIgnf8VU',
    appId: '1:785781696031:ios:421b853679b64e5118972a',
    messagingSenderId: '785781696031',
    projectId: 'gde-app-f74ff',
    storageBucket: 'gde-app-f74ff.appspot.com',
    iosClientId: '785781696031-be4ndrad8nsb6lcjio08sf38tghp24sk.apps.googleusercontent.com',
    iosBundleId: 'com.example.gdeApp',
  );

  static const FirebaseOptions windows = FirebaseOptions(
    apiKey: 'AIzaSyA44Vhja2zn8vyEmS0lytKIXngIRnsVFmU',
    appId: '1:785781696031:web:1d71e2d0252018c418972a',
    messagingSenderId: '785781696031',
    projectId: 'gde-app-f74ff',
    authDomain: 'gde-app-f74ff.firebaseapp.com',
    storageBucket: 'gde-app-f74ff.appspot.com',
  );
}
