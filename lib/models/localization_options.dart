class LocalizationOptions {
  final String? ownerId;

  const LocalizationOptions({this.ownerId});
}
