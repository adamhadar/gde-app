import 'package:gde_app/models/user.dart';

class Comment {
  String id;
  String requrestId;
  User author;
  String content;
  bool isCorrect;
  DateTime createdAt;
  DateTime? markedCorrectAt;

  Comment({
    required this.requrestId,
    required this.id,
    required this.author,
    required this.content,
    required this.isCorrect,
    required this.createdAt,
    this.markedCorrectAt,
  });
}
