class User {
  final String id;
  final String username;
  final String profilePictureUrl;
  int? numberOfRequestsSolved;

  User({
    required this.id,
    required this.username,
    required this.profilePictureUrl,
  });
}
