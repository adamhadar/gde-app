enum OrderType {
  oldest,
  newest,
  mostDiscussed,
}

class FilterOptions {
  OrderType? orderType;
  bool includeSolved = true;
}
