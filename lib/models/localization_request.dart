import 'package:gde_app/models/user.dart';

class LocalizationRequest {
  final String id;
  final String title;
  final String description;
  final String pictureUrl;
  final User author;
  final bool isSolved;
  final DateTime createdOn;
  final List<String>? commentIds = null;

  LocalizationRequest({
    required this.id,
    required this.author,
    required this.title,
    required this.pictureUrl,
    required this.createdOn,
    required this.description,
    required this.isSolved,
  });
}
