import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LogoWidget extends StatelessWidget {
  const LogoWidget({super.key});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width * .3,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 5),
            blurRadius: 8,
            color: Colors.black.withOpacity(.2),
          )
        ],
        color: Colors.white,
        borderRadius: const BorderRadius.all(
          Radius.circular(32),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            "GDE",
            style: TextStyle(
              fontSize: 36,
              letterSpacing: -3,
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.w800,
              shadows: [
                BoxShadow(color: Colors.black.withOpacity(.1) // shadow color
                    ),
                const BoxShadow(
                  offset: Offset(0, 4),
                  blurRadius: 1,
                  color: Color(0xFFF9F8F9), // background color
                ),
              ],
            ),
          ),
          SvgPicture.asset(
            'assets/images/logo_icon.svg',
            width: 32,
            height: 32,
          )
        ],
      ),
    );
  }
}
