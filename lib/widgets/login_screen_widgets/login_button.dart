import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget {
  const LoginButton(this.onPressedFunction, this.labelText, {super.key});

  final Future<void> Function() onPressedFunction;
  final String labelText;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
          height: size.height * .08 < 40 ? size.height * .08 : 40,
          width: size.width * .75,
          color: Theme.of(context).primaryColor,
          child: TextButton(
            onPressed: () async => await onPressedFunction(),
            child: Text(
              labelText,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
