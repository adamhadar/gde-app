import 'dart:developer' as dev;

import 'package:flutter/material.dart';
import 'package:gde_app/providers/auth.dart';
import 'package:provider/provider.dart';

import 'package:gde_app/main.dart';

class LoginGoogleButton extends StatelessWidget {
  const LoginGoogleButton({super.key});
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final height = (size.height * .08 < 40 ? size.height * .08 : 40).toDouble();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: size.width * .1),
        width: size.width * .75,
        height: height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: const Color(0xFF37608F),
        ),
        child: SizedBox(
          height: height,
          width: size.width * .55,
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: TextButton(
              onPressed: () async {
                FocusManager.instance.primaryFocus?.unfocus();
                try {
                  final bool isNew =
                      await Provider.of<AuthProvider>(context, listen: false)
                          .continueWithGoogle();

                  navigationKey.currentState!
                      .pushReplacementNamed(isNew ? '/login2' : '/home');
                } catch (e) {
                  dev.log(e.toString());
                  if (!context.mounted) {
                    return;
                  }
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(e.toString()),
                      backgroundColor: Colors.red,
                    ),
                  );
                }
              },
              child: Row(
                children: [
                  Image.asset(
                    'assets/images/google_logo.png',
                    height: (height / 2).toDouble(),
                  ),
                  SizedBox(width: size.width * .02),
                  const Text(
                    'Pokračovať s Google',
                    softWrap: false,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
