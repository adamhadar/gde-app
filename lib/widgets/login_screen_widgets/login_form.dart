import 'package:gde_app/main.dart';
import 'package:flutter/material.dart';
import 'package:gde_app/providers/auth.dart';
import 'package:gde_app/widgets/login_screen_widgets/input_widget.dart';
import 'package:gde_app/widgets/login_screen_widgets/login_button.dart';
import 'package:gde_app/widgets/login_screen_widgets/login_button_google.dart';
import 'package:gde_app/widgets/login_screen_widgets/or_divider.dart';
import 'package:provider/provider.dart';

class LoginFormBody extends StatefulWidget {
  const LoginFormBody({super.key});

  @override
  State<LoginFormBody> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginFormBody> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _usernameController.dispose();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _emailController.clear();
      _passwordController.clear();
      _usernameController.clear();

      //TODO remove
      _emailController.text = 'adam@test.test';
      _passwordController.text = 'test1234';
    });
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context);

    return Container(
      padding: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height * .05),
      alignment: Alignment.center,
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            TextInputWidget(
              size: MediaQuery.of(context).size,
              validator: (String? x) {
                return RegExp(
                            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,}")
                        .hasMatch(x ?? "")
                    ? null
                    : 'Email je v nesprávnom formáte';
              },
              controller: _emailController,
              labelText: 'Email',
            ),
            const SizedBox(height: 12),
            if (!Provider.of<AuthProvider>(context).getLoggingIn)
              TextInputWidget(
                size: MediaQuery.of(context).size,
                validator: (String? x) {
                  return (x ?? '').length >= 3
                      ? null
                      : 'Používateľské meno musí obsahovať aspoň 3 znaky';
                },
                controller: _usernameController,
                labelText: 'Používateľské meno',
              ),
            const SizedBox(height: 12),
            TextInputWidget(
              size: MediaQuery.of(context).size,
              validator: (String? x) {
                return RegExp(r'(.){6,}').hasMatch(x!)
                    ? null
                    : 'Heslo musí obsahovať aspoň 6 znakov';
              },
              controller: _passwordController,
              labelText: 'Heslo',
              isPassword: true,
            ),
            const SizedBox(height: 24),
            LoginButton(
              loginFunction,
              authProvider.getLoggingIn ? 'Prihlásiť' : 'Registrovať',
            ),
            const OrDivider(label: 'ALEBO', height: 24),
            const LoginGoogleButton(),
            const SizedBox(height: 24),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(authProvider.getLoggingIn
                    ? 'Ešte nemáte účet?'
                    : 'Už máte účet?'),
                const SizedBox(width: 12),
                TextButton(
                  onPressed: authProvider.toggleIsLoggingIn,
                  child: Text(
                    authProvider.getLoggingIn
                        ? 'Zaregistrujte sa'
                        : 'Prihláste sa',
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        color: Theme.of(context).primaryColor),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<void> loginFunction() async {
    FocusManager.instance.primaryFocus?.unfocus();
    final AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    if (_formKey.currentState!.validate()) {
      // Login
      if (authProvider.getLoggingIn) {
        try {
          await authProvider.login(
            email: _emailController.text,
            password: _passwordController.text,
          );

          if (authProvider.loggedIn) {
            await navigationKey.currentState?.pushReplacementNamed('/home');
            return;
          }
        } catch (e) {
          if (context.mounted) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                duration: const Duration(seconds: 2),
                content: Text(e.toString()),
              ),
            );
          }
        }
      }
      // Register
      else {
        authProvider.setEmailAndPass(
            _emailController.text, _passwordController.text);

        await authProvider.register(_usernameController.text);

        if (authProvider.loggedIn) {
          navigationKey.currentState?.pushReplacementNamed('/home');
          return;
        }

        if (!context.mounted) {
          return;
        }

        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            duration: Duration(seconds: 2),
            content: Text('Registrácia zlyhala'),
          ),
        );
      }
    }
  }
}
