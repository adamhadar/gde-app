import 'package:flutter/material.dart';
import 'package:gde_app/models/comment.dart';
import 'package:gde_app/providers/auth.dart';
import 'package:gde_app/providers/requests.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class CommentDisplayBody extends StatelessWidget {
  const CommentDisplayBody({
    super.key,
    required this.comment,
  });

  final Comment comment;

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context);
    final requestProvider = Provider.of<LocalizationRequestProvider>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ConstrainedBox(
          constraints: const BoxConstraints(minHeight: 50, maxWidth: 200),
          child: Text(
            comment.content,
            softWrap: true,
            maxLines: null,
          ),
        ),
        Text(
          style: TextStyle(
            color: Theme.of(context).primaryColor,
          ),
          "zverejnené ${DateFormat('dd.MM.yyyy').format(comment.createdAt)}",
        ),
        if (comment.author.id == authProvider.getUID &&
            !(requestProvider.requests
                    .where((element) => element.id == comment.requrestId)
                    .firstOrNull
                    ?.isSolved ??
                false))
          TextButton(
            onPressed: () async {
              await Provider.of<LocalizationRequestProvider>(
                context,
                listen: false,
              ).markCommentAsCorrect(comment.id);
            },
            child: const Text('Označiť ako správne riešenie'),
          )
      ],
    );
  }
}
