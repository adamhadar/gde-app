import 'package:flutter/material.dart';
import 'package:gde_app/models/localization_request.dart';
import 'package:gde_app/providers/requests.dart';
import 'package:gde_app/widgets/request_detail_screen_widgets/comment_display_body.dart';
import 'package:gde_app/widgets/request_detail_screen_widgets/comment_widget.dart';
import 'package:provider/provider.dart';

class CommentListView extends StatefulWidget {
  const CommentListView({
    super.key,
    required this.request,
  });

  final LocalizationRequest request;

  @override
  State<CommentListView> createState() => _CommentListViewState();
}

class _CommentListViewState extends State<CommentListView> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
      (_) async {
        await Provider.of<LocalizationRequestProvider>(
          context,
          listen: false,
        ).fetchComments();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LocalizationRequestProvider>(
      builder: (_, requestProvider, __) {
        if (requestProvider.isFetching) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        final comments = requestProvider.comments
            .where((element) => element.requrestId == widget.request.id)
            .toList();
        comments.sort((a, b) => a.isCorrect ? -1 : 1);

        return ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: comments.length,
          itemBuilder: (ctx, idx) {
            final comment = comments[idx];
            return CommentWidget(
              comment,
              CommentDisplayBody(comment: comment),
            );
          },
        );
      },
    );
  }
}
