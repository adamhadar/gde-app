import 'package:flutter/material.dart';
import 'package:gde_app/models/comment.dart';
import 'package:gde_app/providers/storage.dart';

class CommentWidget extends StatelessWidget {
  const CommentWidget(
    this.comment,
    this.commentContent, {
    super.key,
  });

  final Comment comment;
  final Widget commentContent;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.white,
        border: Border.all(
          color: Theme.of(context)
              .primaryColor
              .withOpacity(comment.isCorrect ? 1 : 0),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 1,
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            children: [
              CircleAvatar(
                radius: 25,
                backgroundImage: FadeInImage(
                  placeholder: Image.network(
                    StorageProvider.placeholderPfpUrl,
                  ).image,
                  image: Image.network(
                    comment.author.profilePictureUrl,
                  ).image,
                ).image,
              ),
              Text(comment.author.username),
            ],
          ),
          const SizedBox(width: 10),
          commentContent,
        ],
      ),
    );
  }
}
