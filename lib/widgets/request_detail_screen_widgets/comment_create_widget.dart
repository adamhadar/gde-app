import 'package:flutter/material.dart';
import 'package:gde_app/models/comment.dart';
import 'package:gde_app/models/localization_request.dart';
import 'package:gde_app/models/user.dart';
import 'package:gde_app/providers/auth.dart';
import 'package:gde_app/providers/requests.dart';
import 'package:gde_app/widgets/request_detail_screen_widgets/comment_widget.dart';
import 'package:provider/provider.dart';

class CommentCreateWidget extends StatefulWidget {
  const CommentCreateWidget(
    this.request, {
    super.key,
  });

  final LocalizationRequest request;

  @override
  State<CommentCreateWidget> createState() => _CommentCreateWidgetState();
}

class _CommentCreateWidgetState extends State<CommentCreateWidget> {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller.clear();
    _controller.text = "Pridať komentár";
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context);
    final currentUser = User(
      id: authProvider.getUID,
      username: authProvider.getUsername,
      profilePictureUrl: authProvider.getPfpUrl,
    );

    return CommentWidget(
      Comment(
        author: currentUser,
        content: "",
        createdAt: DateTime.now(),
        id: "",
        requrestId: "",
        isCorrect: false,
      ),
      Column(
        children: [
          if (widget.request.isSolved)
            ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 180),
              child: const Center(
                child: Text(
                  "Tento požiadavok je uzavretý, nie je možné pridávať komentáre.",
                  softWrap: true,
                  maxLines: null,
                ),
              ),
            ),
          if (!widget.request.isSolved)
            SizedBox(
              width: 200,
              height: 60,
              child: TextField(
                controller: _controller,
                maxLines: null, // Makes the TextField multiline
                decoration: const InputDecoration(
                  border: InputBorder.none,
                ),
              ),
            ),
          Container(
            width: 225,
            alignment: Alignment.centerRight,
            child: TextButton(
              style: ButtonStyle(
                padding: MaterialStateProperty.all(const EdgeInsets.all(0)),
              ),
              onPressed: widget.request.isSolved
                  ? null
                  : () async {
                      debugPrint("Creating comment");
                      await Provider.of<LocalizationRequestProvider>(
                        context,
                        listen: false,
                      ).createComment(
                        text: _controller.text,
                        authorId: authProvider.getUID,
                        requestId: widget.request.id,
                      );

                      debugPrint("Comment created");
                    },
              child: Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 5,
                ),
                margin: const EdgeInsets.all(0),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(32),
                ),
                child: const Text(
                  "Odoslať",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w800,
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
