import 'package:flutter/material.dart';
import 'package:gde_app/main.dart';
import 'package:gde_app/providers/auth.dart';
import 'package:gde_app/providers/requests.dart';
import 'package:gde_app/widgets/home_screen_widgets/my_icon_button.dart';
import 'package:gde_app/widgets/logo_widget.dart';
import 'package:provider/provider.dart';

class NavBarRow extends StatelessWidget {
  const NavBarRow({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final requestProvider = Provider.of<LocalizationRequestProvider>(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        const LogoWidget(),
        const SizedBox.shrink(),
        const SizedBox.shrink(),
        MyIconButton(
          icon: Icons.filter_list,
          onTap: () => requestProvider.toggleFiltering(),
        ),
        MyIconButton(
          icon: Icons.bar_chart_outlined,
          onTap: () {
            navigationKey.currentState!.pushNamed('/leaderboard');
          },
        ),
        MyIconButton(
          icon: Icons.person_outline,
          onTap: () {
            navigationKey.currentState!.pushNamed('/profile');
          },
        ),
        MyIconButton(
          icon: Icons.logout_outlined,
          onTap: () {
            final authProvider =
                Provider.of<AuthProvider>(context, listen: false);
            authProvider.loggedIn = false;
            navigationKey.currentState!.pushReplacementNamed('/');
          },
        )
      ],
    );
  }
}
