import 'package:flutter/material.dart';

class MyIconButton extends StatelessWidget {
  const MyIconButton({
    super.key,
    required this.icon,
    required this.onTap,
  });

  final VoidCallback onTap;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 40,
        width: 40,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 5),
              blurRadius: 8,
              color: Colors.black.withOpacity(.2),
            )
          ],
          color: Colors.white,
          borderRadius: const BorderRadius.all(
            Radius.circular(32),
          ),
        ),
        child: Icon(
          icon,
          color: Theme.of(context).primaryColor,
          size: 30,
        ),
      ),
    );
  }
}
