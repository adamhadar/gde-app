import 'package:flutter/material.dart';
import 'package:gde_app/main.dart';
import 'package:gde_app/models/filter_options.dart';
import 'package:gde_app/models/localization_options.dart';
import 'package:gde_app/providers/requests.dart';
import 'package:intl/intl.dart';

class RequestListView extends StatelessWidget {
  const RequestListView(
    this.requestProvider, {
    super.key,
    this.options = const LocalizationOptions(),
  });

  final LocalizationRequestProvider requestProvider;
  final LocalizationOptions options;
  @override
  Widget build(BuildContext context) {
    var requests = requestProvider.requests;
    if (options.ownerId != null) {
      requests = requests
          .where((element) =>
              options.ownerId == null || element.author.id == options.ownerId)
          .toList();
    } else if (requestProvider.filtering) {
      if (!requestProvider.filterOptions.includeSolved) {
        requests = requests.where((element) => !element.isSolved).toList();
      }

      switch (requestProvider.filterOptions.orderType) {
        case OrderType.newest:
          requests.sort((a, b) => b.createdOn.compareTo(a.createdOn));
        case OrderType.oldest:
          requests.sort((a, b) => a.createdOn.compareTo(b.createdOn));

        default:
          break;
      }
    }

    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: requests.length,
      itemBuilder: (context, index) {
        final request = requests[index];
        return Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            // Content
            child: Column(
              children: [
                // Title Row
                Row(
                  children: [
                    Text(
                      request.title,
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    const Spacer(),
                    if (request.isSolved)
                      const Icon(
                        Icons.check_circle,
                        size: 32,
                        color: Colors.green,
                      ),
                  ],
                ),
                // Body row
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(16),
                      child: Image.network(
                        request.pictureUrl,
                        height: 100,
                        width: 100,
                        fit: BoxFit.cover,
                      ),
                    ),
                    const SizedBox(width: 10),
                    // Description Column
                    ConstrainedBox(
                      constraints:
                          const BoxConstraints(maxHeight: 110, maxWidth: 250),
                      child: Column(
                        children: [
                          SizedBox(
                            width: 250,
                            child: Text(
                              overflow: TextOverflow.ellipsis,
                              softWrap: true,
                              maxLines: 3,
                              request.description,
                              style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          const Spacer(),
                          Row(
                            children: [
                              Text(
                                "Zverejnené ${DateFormat('dd.MM.yyyy').format(request.createdOn)}",
                              ),
                              const Spacer(),
                              InkWell(
                                onTap: () {
                                  navigationKey.currentState!.pushNamed(
                                    '/request',
                                    arguments: request.id,
                                  );
                                },
                                borderRadius: BorderRadius.circular(4),
                                overlayColor: MaterialStateProperty.all(
                                    Theme.of(context)
                                        .primaryColor
                                        .withOpacity(.2)),
                                child: Text(
                                  "viac...",
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                if (options.ownerId != null)
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                          horizontal: 5,
                        ),
                        child: GestureDetector(
                          onTap: () {
                            requestProvider.currentlyEditing = request;
                            navigationKey.currentState!.pushNamed('/new').then(
                                (value) => requestProvider.fetchRequests());
                          },
                          child: const Icon(
                            Icons.edit_outlined,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                      const Spacer(),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                          horizontal: 5,
                        ),
                        child: GestureDetector(
                          onTap: () async {
                            await showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: const Text('Confirmation'),
                                  content: const Text(
                                      'Are you sure you want to delete this request?'),
                                  actions: [
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.of(context).pop(),
                                      child: const Text('Cancel'),
                                    ),
                                    TextButton(
                                      onPressed: () async {
                                        await requestProvider
                                            .deleteRequest(request.id);
                                        Navigator.of(context).pop();
                                      },
                                      child: const Text(
                                        'Delete',
                                        style: TextStyle(color: Colors.red),
                                      ),
                                    ),
                                  ],
                                );
                              },
                            ).then((value) => requestProvider.fetchRequests());
                          },
                          child: const Icon(
                            Icons.delete_outline,
                            color: Colors.red,
                          ),
                        ),
                      ),
                    ],
                  ),
              ],
            ),
          ),
        );
      },
    );
  }
}
