import 'package:flutter/material.dart';

class DescriptionInput extends StatelessWidget {
  const DescriptionInput({
    super.key,
    required TextEditingController descriptionController,
    required OutlineInputBorder outlineInputBorder,
  })  : _descriptionController = descriptionController,
        _outlineInputBorder = outlineInputBorder;

  final TextEditingController _descriptionController;
  final OutlineInputBorder _outlineInputBorder;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: TextField(
        controller: _descriptionController,
        maxLines: 5,
        decoration: InputDecoration(
          border: const OutlineInputBorder().copyWith(
            borderRadius: const BorderRadius.all(
              Radius.circular(16),
            ),
          ),
          enabledBorder: _outlineInputBorder,
          focusedBorder: _outlineInputBorder,
          hintText: 'Pridať popis...',
        ),
      ),
    );
  }
}
