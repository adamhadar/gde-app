import 'package:flutter/material.dart';
import 'package:gde_app/main.dart';
import 'package:gde_app/providers/auth.dart';
import 'package:gde_app/providers/requests.dart';
import 'package:gde_app/providers/storage.dart';
import 'package:gde_app/widgets/new_requrest_screen_widgets/description_input.dart';
import 'package:gde_app/widgets/new_requrest_screen_widgets/select_image_tappable.dart';
import 'package:gde_app/widgets/primary_color_text_button.dart';
import 'package:provider/provider.dart';

class NewRequestForm extends StatefulWidget {
  const NewRequestForm({super.key});

  @override
  State<NewRequestForm> createState() => _NewRequestFormState();
}

class _NewRequestFormState extends State<NewRequestForm> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final OutlineInputBorder _outlineInputBorder = OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.black.withOpacity(0.2),
      width: 1,
    ),
    borderRadius: const BorderRadius.all(
      Radius.circular(16),
    ),
  );

  @override
  void initState() {
    super.initState();
    _titleController.text = "Nazov";
    _descriptionController.clear();

    final requestProvider = Provider.of<LocalizationRequestProvider>(
      context,
      listen: false,
    );

    if (requestProvider.currentlyEditing != null) {
      final request = requestProvider.currentlyEditing!;
      _titleController.text = request.title;
      _descriptionController.text = request.description;
      Provider.of<StorageProvider>(
        context,
        listen: false,
      );
    }

    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<StorageProvider>(
        context,
        listen: false,
      ).lastSelectedFile = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    final requestProvider = Provider.of<LocalizationRequestProvider>(
      context,
      listen: false,
    );
    final authProvider = Provider.of<AuthProvider>(
      context,
      listen: false,
    );
    final storageProvider = Provider.of<StorageProvider>(
      context,
      listen: false,
    );
    return Form(
      key: _formKey,
      child: Card(
        surfaceTintColor: Colors.white,
        margin: const EdgeInsets.all(10),
        elevation: 5,
        child: Column(
          children: [
            // Editable text - Title
            Padding(
              padding: const EdgeInsets.all(10),
              child: EditableText(
                controller: _titleController,
                focusNode: FocusNode(),
                style: const TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.w800,
                  color: Colors.black,
                ),
                cursorColor: Theme.of(context).primaryColor,
                cursorWidth: 2,
                backgroundCursorColor: Colors.white,
                onChanged: (value) {
                  debugPrint(value);
                },
              ),
            ),
            const SizedBox(height: 10),
            const SelectImageTappable(),
            const SizedBox(height: 10),

            // Container with text - Description
            Container(
              width: double.infinity,
              padding: const EdgeInsets.only(left: 10),
              child: const Text(
                "Popis:",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w800,
                  color: Colors.black,
                ),
                textAlign: TextAlign.start,
              ),
            ),

            // Description input
            DescriptionInput(
              descriptionController: _descriptionController,
              outlineInputBorder: _outlineInputBorder,
            ),
            const SizedBox(height: 10),

            // Submit button
            PrimaryColorTextButton(
              text: requestProvider.currentlyEditing != null
                  ? "Uložiť žiadosť"
                  : "Zverejniť žiadosť",
              onPressed: () async {
                final isEditing = requestProvider.currentlyEditing != null;
                if (!_formKey.currentState!.validate()) {
                  return;
                }

                if (storageProvider.lastSelectedFile == null && !isEditing) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Vyberte obrázok"),
                    ),
                  );
                  return;
                }

                if (storageProvider.lastSelectedFile != null &&
                    !await storageProvider.uploadFile(
                      authProvider.getUID,
                      storageProvider.lastSelectedFile!,
                    )) {
                  if (context.mounted) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text("Chyba pri nahrávaní súboru"),
                      ),
                    );
                  }
                  return;
                }
                if (isEditing) {
                  await requestProvider.updateRequest(
                    id: requestProvider.currentlyEditing!.id,
                    title: _titleController.text,
                    description: _descriptionController.text,
                    pictureUrl: storageProvider.lastSelectedUrl,
                  );
                } else {
                  await requestProvider.createRequest(
                    title: _titleController.text,
                    description: _descriptionController.text,
                    authorId: authProvider.getUID,
                    pictureUrl: storageProvider.lastSelectedUrl,
                  );
                }

                storageProvider.lastSelectedFile = null;
                navigationKey.currentState!.pop();
              },
            )
          ],
        ),
      ),
    );
  }
}
