import 'dart:developer' as developer;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:gde_app/exceptions/auth_exception.dart';
import 'package:gde_app/providers/storage.dart';
import 'package:google_sign_in/google_sign_in.dart';

/// The AuthProvider class is used to handle all authentification
/// during the apps lifecycle.
///
/// Allows authentification with either email+password combination
/// or via the google_sign_in.
/// Also Handles the creation of data after registration.
class AuthProvider with ChangeNotifier {
  /// The uid of the currently logged in user.
  late String _uid;

  /// Get the UID of the currently logged in user.
  String get getUID => _uid;

  /// The email of the current user.
  /// The user doesn't have to be logged in, this field is also used when
  /// registering/loggin in.
  late String _email;

  /// Get the email of the current (not necessarily logged in) user.
  String get getEmail => _email;

  late String _username;

  // Get the users username
  String get getUsername => _username;

  /// The password of the user that is currently registering/logging in.
  late String _password;

  /// Set the [email] and [password] of the current user in the provider.
  void setEmailAndPass(String email, String password) {
    _email = email;
    _password = password;
  }

  /// Whether the user is logging in or registering, the default is logging in.
  bool _isLoggingIn = true;

  /// Toggle the logging in state, the default value is true.
  void toggleIsLoggingIn() {
    _isLoggingIn = !_isLoggingIn;
    notifyListeners();
  }

  /// Returns `true` if the user is logging in, `false` for registering.
  bool get getLoggingIn => _isLoggingIn;

  /// Whether the user is currently logged in or not.
  bool loggedIn = false;

  /// Whether or not the user was authentificated via google-sign-in
  bool _authentificatedGoogle = false;

  /// Holds a reference to the apps `FirebaseAuth` instance.
  final FirebaseAuth _auth = FirebaseAuth.instance;

  /// Holds a reference to the `FirebaseFirestore` instance.
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  /// Returns the current user, if there is a currently logged in user.
  /// `null` otherwise.
  User? getCurrentUser() => _auth.currentUser;

  late final String _pfpUrl;

  get getPfpUrl => _pfpUrl;

  /// Checks if a record for the current user exists in the `FireStore` databse.
  /// Return `true` if it does, `false` otherwise.
  Future<bool> firestoreDocExists() async {
    try {
      var doc = await _firestore.collection('users').doc(_uid).get();
      return doc.exists;
    } catch (e) {
      developer.log(e.toString());
      return false;
    }
  }

  /// Tries to change the password of the currently
  /// logged in user to [password].
  Future<bool> changePassword(String password) async {
    User? user = getCurrentUser();
    bool rv = false;

    // Verify that there is currently a logged in user.
    if (user == null) {
      return false;
    }

    await user.updatePassword(password).then((_) {
      developer.log("Successfully changed password");
      rv = true;
    }).catchError((error) {
      developer.log("Password can't be changed $error");
    });

    return rv;
  }

  /// Tries to register using a [email] and [password] combination.
  /// This function also sets the `uid` and `email` fields of this provider.
  /// Returns a Future yielding true if the registration was successful,
  /// `false` otherwise.
  Future<bool> _firebaseRegisterEmail(String email, String password) async {
    try {
      final userData = await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      // Forcefuly remove the nullability, if any of the nullable fields is
      // null,this code throws an exception and goes into the catch branch.
      _uid = userData.user!.uid;
      _email = userData.user!.email!;

      return true;
    } catch (e) {
      developer.log('ERROR: $e');
      return false;
    }
  }

  /// Tries to log in using the [email] and [password] combination.
  /// This function also sets the `uid` and `email` fields of this provider.
  /// Returns a Future yielding true if the registration was successful,
  /// `false` otherwise.
  ///
  /// Throws an `FirebaseAuthException` if the login fails.
  Future<bool> _firebaseLoginEmail(String email, String password) async {
    try {
      final UserCredential userData = await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      _uid = userData.user!.uid;
      _email = userData.user!.email!;

      return true;
    } catch (e) {
      throw (AuthException(e.toString()));
    }
  }

  /// Creates the data of a user after his registration.
  Future<void> createDataAfterReg(String username) async {
    _username = username;
    _pfpUrl = StorageProvider.placeholderPfpUrl;
    bool firestoreExists = await firestoreDocExists();

    if (!firestoreExists) {
      await _firestore.collection('users').doc(_uid).set(
        {
          'username': username,
          'imageUrl': _pfpUrl,
        },
      );
    }
  }

  /// Hangle user login
  /// Sets the field [loggedIn] to the success of this operation.\
  /// Throws an AuthException.
  Future<void> login({
    required String email,
    required String password,
  }) async {
    loggedIn = await _firebaseLoginEmail(email, password);
    if (!loggedIn) {
      return;
    }

    CollectionReference users = _firestore.collection('users');
    final DocumentSnapshot docSnap = await users.doc(getUID).get();
    final Map<String, dynamic> data = docSnap.data() as Map<String, dynamic>;
    _username = data['username'];
    _pfpUrl = data['imageUrl'];
  }

  /// Handles registration of a user.
  ///
  /// This meethod is called regardless of the authentification method used.
  /// It calls the `createDataAfterReg` method.
  Future<void> register(String username) async {
    // If google-sign-in was used, no further actions needed.
    // Otherwise register to firebase separately
    _username = username;
    loggedIn = _authentificatedGoogle
        ? true
        : await _firebaseRegisterEmail(_email, _password);
    if (loggedIn) {
      await createDataAfterReg(username);
    }
    notifyListeners();
  }

  /// Utilize the google-sign-in package to handle authentification.
  /// Returns a promise which yields true if the user is new, false otherwise.
  /// Throws an `AuthException` if the sign-in fails.
  Future<bool> continueWithGoogle() async {
    final authUser = await GoogleSignIn().signIn();

    if (authUser == null) {
      throw AuthException('Prihlásenie cez google zlyhalo');
    }

    final googleAuth = await authUser.authentication;

    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final userData = await _auth.signInWithCredential(credential);

    // Remvoe nullability by checking if the user is null
    final User? user = userData.user;

    if (user == null) {
      throw AuthException('Prihlásenie cez google zlyhalo');
    }

    _uid = user.uid;
    _email = user.email!;

    final AdditionalUserInfo? userInfo = userData.additionalUserInfo;

    if (userInfo == null) {
      throw AuthException('Prihlásenie cez google zlyhalo');
    }

    if (!userInfo.isNewUser) {
      CollectionReference users = _firestore.collection('users');
      final DocumentSnapshot docSnap = await users.doc(getUID).get();
      final Map<String, dynamic> data = docSnap.data() as Map<String, dynamic>;
      _username = data['username'];
      _pfpUrl = data['imageUrl'];
    } else {
      createDataAfterReg(userInfo.username ?? "Jane Doe");
    }

    _authentificatedGoogle = true;
    return userInfo.isNewUser;
  }

  Future<void> updateUsername(String username) async {
    debugPrint('Updating username to $username');
    await _firestore.collection('users').doc(_uid).update({
      'username': username,
    });

    _username = username;
    notifyListeners();
  }
}
