import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class StorageProvider with ChangeNotifier {
  // Create a storage reference from our app
  final storageRef = FirebaseStorage.instance.ref();

  // Store the last selected file when creating a new request.
  File? _lastSelectedFile;

  File? get lastSelectedFile => _lastSelectedFile;

  set lastSelectedFile(File? file) {
    _lastSelectedFile = file;
    notifyListeners();
  }

  String? _lastUploadedURL;

  static String placeholderUrl =
      r'https://firebasestorage.googleapis.com/v0/b/gde-app-f74ff.appspot.com/o/600x400%5B1%5D.png?alt=media&token=86851cd5-698c-45b6-be4a-3e862b928ede';
  static String placeholderPfpUrl =
      r'https://firebasestorage.googleapis.com/v0/b/gde-app-f74ff.appspot.com/o/pfp_placeholder.jpg?alt=media&token=6e0a0ec9-91a4-4af3-9914-25e41f6916e9';

  String get lastSelectedUrl => _lastUploadedURL ?? placeholderUrl;

  Future<bool> uploadFile(String uid, File file) async {
    try {
      // Create a child reference under the storage reference
      final userRef = storageRef.child('users/$uid/file_name');

      // Upload the file to Firebase Storage
      final ref = await userRef.putFile(file);
      // Get the download URL of the uploaded file
      _lastUploadedURL = await ref.ref.getDownloadURL();
      return true;
    } catch (error) {
      debugPrint('Error uploading file: $error');
      return false;
    }
  }
}
