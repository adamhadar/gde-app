import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:gde_app/models/comment.dart';
import 'package:gde_app/models/filter_options.dart';
import 'package:gde_app/models/user.dart';
import 'package:gde_app/models/localization_request.dart';
import 'package:gde_app/providers/storage.dart';

class LocalizationRequestProvider with ChangeNotifier {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  List<LocalizationRequest> requests = [];

  List<Comment> comments = [];

  List<User> leaderboard = [];

  LocalizationRequest? currentlyEditing;

  bool _isFetching = false;

  bool get isFetching => _isFetching;

  bool _filtering = false;

  bool get filtering => _filtering;

  void toggleFiltering() {
    _filtering = !_filtering;
    notifyListeners();
  }

  FilterOptions filterOptions = FilterOptions();

  void setFilterOptions(bool? includeSolved, OrderType? orderType) {
    if (includeSolved != null) {
      filterOptions.includeSolved = includeSolved;
    }
    if (orderType != null) {
      filterOptions.orderType = orderType;
    }

    notifyListeners();
  }

  void toggleIsFetching() {
    _isFetching = !_isFetching;
    notifyListeners();
  }

  Future<bool> createRequest({
    required String title,
    required String description,
    required String authorId,
    required String pictureUrl,
  }) async {
    final data = {
      "title": title,
      "description": description,
      "authorId": _firestore.doc('users/$authorId'),
      "pictureUrl": pictureUrl,
    };
    final documentReference = await _firestore.collection('requests').add(data);

    documentReference.get().then((value) {
      debugPrint(value.data().toString());
    });

    return true;
  }

  Future<void> fetchRequests() async {
    this.requests.clear();
    toggleIsFetching();

    final requests = await _firestore.collection('requests').get();
    for (var request in requests.docs) {
      final requestData = request.data();
      final userRef = await (requestData['authorId']
              as DocumentReference<Map<String, dynamic>>)
          .get();
      final userData = userRef.data() ?? {};

      this.requests.add(
            LocalizationRequest(
              id: request.id,
              createdOn: DateTime.now(),
              title: requestData['title'],
              description: requestData['description'],
              pictureUrl: requestData['pictureUrl'],
              author: User(
                id: userRef.id,
                username: userData['username'] ?? 'Deleted User',
                profilePictureUrl:
                    userData['imageUrl'] ?? StorageProvider.placeholderPfpUrl,
              ),
              isSolved: requestData['isSolved'] ?? false,
            ),
          );
    }

    toggleIsFetching();
  }

  Future<void> fetchComments() async {
    this.comments.clear();
    toggleIsFetching();

    final comments = await _firestore.collection('comments').get();
    for (var comment in comments.docs) {
      final commentData = comment.data();
      final userRef = await (commentData['author']
              as DocumentReference<Map<String, dynamic>>)
          .get();
      final userData = userRef.data() ?? {};
      final requestId = commentData['request'].id;

      this.comments.add(
            Comment(
              id: comment.id,
              requrestId: requestId,
              content: commentData['text'],
              isCorrect: commentData['isCorrect'],
              createdAt: commentData['createdOn'].toDate(),
              markedCorrectAt: commentData['markedCorrectAt']?.toDate(),
              author: User(
                id: userRef.id,
                username: userData['username'] ?? 'Deleted User',
                profilePictureUrl:
                    userData['imageUrl'] ?? StorageProvider.placeholderPfpUrl,
              ),
            ),
          );
    }
    toggleIsFetching();
  }

  Future<void> updateRequest({
    required String id,
    required String title,
    required String description,
    required String pictureUrl,
  }) async {
    toggleIsFetching();
    final data = {
      "title": title,
      "description": description,
      "pictureUrl": pictureUrl,
    };

    await _firestore.doc('requests/$id').update(data);
    toggleIsFetching();
  }

  Future<void> createComment({
    required String text,
    required String authorId,
    required String requestId,
  }) async {
    toggleIsFetching();
    final data = {
      "text": text,
      "author": _firestore.doc('users/$authorId'),
      "request": _firestore.doc('requests/$requestId'),
      "createdOn": DateTime.now(),
      "isCorrect": false,
    };

    await _firestore.collection('comments').add(data);
    toggleIsFetching();
  }

  Future<void> markCommentAsCorrect(String commentId) async {
    toggleIsFetching();
    final comment = await _firestore.doc('comments/$commentId').get();
    final commentData = comment.data() as Map<String, dynamic>;
    final requestId = commentData['request'].id;

    await _firestore.doc('requests/$requestId').update(
      {'isSolved': true},
    );

    final data = {
      "isCorrect": true,
      "markedCorrectAt": DateTime.now(),
    };

    await _firestore.doc('comments/$commentId').update(data);
    toggleIsFetching();
  }

  Future<void> deleteRequest(String requestId) async {
    toggleIsFetching();
    await _firestore.doc('requests/$requestId').delete();
    toggleIsFetching();
  }

  Future<void> fetchLeaderboard() async {
    leaderboard.clear();
    toggleIsFetching();

    final users = await _firestore.collection('users').get();
    for (var user in users.docs) {
      final userData = user.data();
      final numberOfComments = await _firestore
          .collection('comments')
          .where('isCorrect', isEqualTo: true)
          .where('author', isEqualTo: user.reference)
          .count()
          .get();

      leaderboard = leaderboard +
          [
            User(
              id: user.id,
              username: userData['username'] ?? 'Deleted User',
              profilePictureUrl:
                  userData['imageUrl'] ?? StorageProvider.placeholderPfpUrl,
            )..numberOfRequestsSolved = numberOfComments.count,
          ];

      leaderboard = leaderboard
          .where((element) => element.numberOfRequestsSolved != null)
          .toList();

      leaderboard.sort((a, b) =>
          b.numberOfRequestsSolved!.compareTo(a.numberOfRequestsSolved!));

      leaderboard = leaderboard.take(5).toList();
    }

    toggleIsFetching();
  }
}
